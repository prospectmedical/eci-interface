#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
'Imports Microsoft.VisualBasic.FileIO
'Imports System.Data.OleDb
Imports Oracle.ManagedDataAccess.Client
#End Region

Module ServiceRoutines
#Region "Declarations"
	Private moProcessHistory As New ProcessHistory
	Private msErrorMessage, msProcessDate, msPathFileName, msCurrentProcessTime, msStartDate, msEndDate As String
	Private mdtCurrentDateTime As Date
	Private miRecordsRead, miRecordsWritten As Int32
#End Region

#Region "Routines"
	Private Function GEBillingDataSQL() As String
		Dim sEndDate As String = msEndDate

		If IsDate(sEndDate) Then sEndDate = Format(DateAdd(DateInterval.Day, 1, CDate(sEndDate)), "yyyy-MM-dd")
		Return "SELECT " & Chr(34) & "DOCUMENT" & Chr(34) & ".DB_CREATE_DATE AS DateOfService, ORDERS.ORDERDATE AS OrderDate, LOCREG.SEARCHNAME AS PracticeName, PERSON.SEARCHNAME AS PATIENTNAME, " &
			"PERSON.EXTERNALID AS MedicalRecordNumber, PERSON.MEDRECNO, " & Chr(34) & "DOCUMENT" & Chr(34) & ".VISITID AS VisitID, ORDERS.ORDERID AS OrderID, ORDERS.CODE AS OrderCode, " &
			"ORDERS.DESCRIPTION AS OrderDescription, ORDERS.NUMVISITS AS NumberOfVisits, " &
			"LISTAGG(ORDER_ENTRY_MODIFIERS.CODE, '~') WITHIN GROUP (ORDER BY ORDERS.ORDERID) AS Modifiers, " &
			"LISTAGG(ORDDX.RANK || '-' || MASTERDIAGNOSIS.CODE, '|') WITHIN GROUP (ORDER BY ORDERS.ORDERID) AS DiagnosisCodes, " &
			"ORDERS.STATUS, AUTHUSRINFO.LASTNAME AS RenderingProviderLastName, " &
			"AUTHUSRINFO.FIRSTNAME AS RenderingProviderFirstName, AUTHUSRINFO.NPI AS RenderingProviderNPI, SIGNEDUSRINFO.LASTNAME AS OrderingProviderLastName, " &
			"SIGNEDUSRINFO.FIRSTNAME AS OrderingProviderFirstName, SIGNEDUSRINFO.NPI AS OrderingProviderNPI, ORDERS.DTS_EXPORT_DATE AS DTSExportDate " &
			"FROM PERSON " &
			"INNER JOIN ORDERS ON PERSON.PID = ORDERS.PID " &
			"INNER JOIN " & Chr(34) & "DOCUMENT" & Chr(34) & " ON ORDERS.SDID = " & Chr(34) & "DOCUMENT" & Chr(34) & ".SDID " &
			"LEFT OUTER JOIN ORDDX ON ORDERS.DXGROUPID = ORDDX.DXGROUPID " &
			"LEFT OUTER JOIN MASTERDIAGNOSIS ON ORDDX.ICD10MASTERDIAGNOSISID = MASTERDIAGNOSIS.MASTERDIAGNOSISID " &
			"LEFT OUTER JOIN ORDER_ENTRY_MODIFIERS ON ORDERS.ORDERID = ORDER_ENTRY_MODIFIERS.ORDERID " &
			"INNER JOIN USRINFO AUTHUSRINFO ON ORDERS.AUTHBYUSRID = AUTHUSRINFO.PVID " &
			"INNER JOIN LOCREG ON " & Chr(34) & "DOCUMENT" & Chr(34) & ".LOCOFCARE = LOCREG.LOCID " &
			"LEFT OUTER JOIN USRINFO SIGNEDUSRINFO ON ORDERS.USRID = SIGNEDUSRINFO.PVID " &
			"WHERE ORDERS.ORDERTYPE = 'S' AND ORDERS." & Chr(34) & "CHANGE" & Chr(34) & " IN(0, 2) AND ORDERS.STATUS = 'C' AND PERSON.PSTATUS = 'A' AND " &
			"ORDERS.DTS_EXPORT_DATE >= to_date('" & msStartDate & "', 'YYYY-MM-DD') AND ORDERS.DTS_EXPORT_DATE < to_date('" & sEndDate & "', 'YYYY-MM-DD') " &
			"GROUP BY " & Chr(34) & "DOCUMENT" & Chr(34) & ".DB_CREATE_DATE, ORDERS.ORDERDATE, LOCREG.SEARCHNAME, PERSON.SEARCHNAME, PERSON.EXTERNALID, PERSON.MEDRECNO, " &
			Chr(34) & "DOCUMENT" & Chr(34) & ".VISITID, ORDERS.ORDERID, ORDERS.CODE, ORDERS.DESCRIPTION, ORDERS.NUMVISITS, ORDERS.STATUS, AUTHUSRINFO.LASTNAME, AUTHUSRINFO.FIRSTNAME, " &
			"AUTHUSRINFO.NPI, SIGNEDUSRINFO.LASTNAME, SIGNEDUSRINFO.FIRSTNAME, SIGNEDUSRINFO.NPI, ORDERS.DTS_EXPORT_DATE " &
			"ORDER BY ORDERS.ORDERDATE, LOCREG.SEARCHNAME, PERSON.SEARCHNAME"

		'"AND LOCREG.ABBREVNAME IN('MIDD', 'G112', 'G112BL', 'G112SP', 'G50-SF', 'G50-BL') " &
	End Function

	Private Sub AddField(ByRef Record As String, Field As String)
		If Len(Record) > 0 Then Record = Record & ","
		Record = Record & Chr(34) & Field & Chr(34)
	End Sub

	Private Function RemoveDuplicates(InField As String, Delimitor As Char) As String
		Dim asFields() As String
		Dim sWork As String
		Dim alFields As New ArrayList
		Dim iWork, iWork1 As Int32
		Dim bFound As Boolean

		If Len(InField) <= 0 Then Return vbNullString
		If Len(Delimitor) <= 0 Then Return InField
		If InStr(1, InField, Delimitor) <= 0 Then Return InField
		asFields = InField.Split(Delimitor)
		RemoveDuplicates = vbNullString
		For iWork = 0 To asFields.Count - 1
			sWork = SetString(asFields(iWork))
			If Len(sWork) > 0 Then
				If alFields.Count > 0 Then
					bFound = False
					For iWork1 = 0 To alFields.Count - 1
						If SetString(alFields.Item(iWork1).ToString) = sWork Then
							bFound = True
							Exit For
						End If
					Next iWork1
					If Not bFound Then
						If Len(RemoveDuplicates) > 0 Then RemoveDuplicates = RemoveDuplicates & Delimitor
						RemoveDuplicates = RemoveDuplicates & sWork
						alFields.Add(sWork)
					End If
				Else
					RemoveDuplicates = sWork
					alFields.Add(sWork)
				End If
			End If
		Next iwork
	End Function
#End Region

#Region "StartService"
	Public Sub StartService(InTextWriterTraceListener As TextWriterTraceListener)
		Dim sRoutine As String = "StartService"
		Dim oFile As StreamWriter = Nothing
		Dim oServicesToRun() As System.ServiceProcess.ServiceBase
		Dim oStartup As New StartUp

		WriteEventLog("Service main method starting at " & Format$(Now, "MM/dd/yyyy HH:mm"), sRoutine, EventLogEntryType.Information)
		If oStartup.Run Then
			CreateDirectory(gsLogPath)
			gsLogFileName = gsApplicationName & " Log " & Format$(Now, "yyyyMMdd HHmmss") & ".txt"
			oFile = File.CreateText(gsLogPath & gsLogFileName)
			InTextWriterTraceListener = New TextWriterTraceListener(oFile)
			Trace.Listeners.Add(InTextWriterTraceListener)
			Trace.AutoFlush = True
			TraceWrite("Startup.Run was initialized...", sRoutine)
			TraceWrite("Application Name: " & gsApplicationName, sRoutine)
			TraceWrite("Windows UserID: " & gsWindowsUserID, sRoutine)
			TraceWrite("Entity Description: " & gsEntityDescription, sRoutine)
			TraceWrite("Application Version: " & gsVersion, sRoutine)
			TraceWrite("Application Startup Path: " & gsAppPath, sRoutine)
			TraceWrite("Service Pause Time: " & Format$(giServicePauseTime \ 60000, "#,##0") & " minutes", sRoutine)
			TraceWrite("EMail Recipient: " & gsEMailRecipient, sRoutine)
			TraceWrite("EMail Sender: " & gsEMailSender, sRoutine)
			If gbSendEMail Then
				TraceWrite("Send EMail: True", sRoutine)
			Else
				TraceWrite("Send EMail: False", sRoutine)
			End If
			TraceWrite("Upload File Location: " & gsUploadFileLocation, sRoutine)
			TraceWrite("Upload File Name: " & gsUploadFileName, sRoutine)
			If galProcessTime.Count > 0 Then
				For iWork = 0 To galProcessTime.Count - 1
					TraceWrite("Processing Time(" & Format$(iWork + 1, "#,##0") & "): " & galProcessTime.Item(iWork).ToString, sRoutine)
				Next iWork
			End If
			If Not gbTrace Then
				gbTrace = True
				TraceWrite("Trace is disabled", sRoutine)
				gbTrace = False
			End If
			WriteEventTrace("Startup Complete", sRoutine, EventLogEntryType.Information)
			TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
			TraceWrite(StringRepeat("-", 100), sRoutine)
			TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
			TraceWrite(Space$(1), sRoutine)
		Else
			WriteEventTrace("Startup.Run failed to initialize...", sRoutine, EventLogEntryType.Error)
			WriteEventTrace(oStartup.ErrorMessage, sRoutine, EventLogEntryType.Error)
			End
		End If
		oStartup = Nothing
		oServicesToRun = New System.ServiceProcess.ServiceBase() {New ECI}
		System.ServiceProcess.ServiceBase.Run(oServicesToRun)
		WriteEventTrace("Service main method exiting...", sRoutine, EventLogEntryType.Information)
		Trace.Listeners.Remove(InTextWriterTraceListener)
		InTextWriterTraceListener.Close()
		InTextWriterTraceListener = Nothing
		oFile.Close()
	End Sub
#End Region

#Region "ProcessMain"
	Public Sub ProcessMain()
		Dim sRoutine As String = "ProcessMain"
		Dim iProcessTypeID As Int32 = 1
		Dim iWork, iDayCounter As Int32
		Dim sStartDate As String
		Dim bProcessDate As Boolean

		Try
			TraceWrite("Start Processing", sRoutine)
			If galProcessTime.Count > 0 Then
				For iWork = 0 To galProcessTime.Count - 1
					msCurrentProcessTime = galProcessTime.Item(iWork).ToString
					mdtCurrentDateTime = Now
					msProcessDate = Format(mdtCurrentDateTime, "yyyyMMdd")
					If Not moProcessHistory.Complete(iProcessTypeID, msProcessDate, msCurrentProcessTime) Then

						For iDayCounter = 50 To 1 Step -1
							sStartDate = Format(DateAdd(DateInterval.Day, iDayCounter * -1, mdtCurrentDateTime), "yyyy-MM-dd")
							If iDayCounter > 1 Then
								If moProcessHistory.DateComplete(iProcessTypeID, sStartDate) Then
									bProcessDate = False
								Else
									bProcessDate = True
								End If
							Else
								bProcessDate = True
							End If
							If bProcessDate Then
								If Not moProcessHistory.MarkStart(iProcessTypeID, mdtCurrentDateTime) Then Throw New Exception(moProcessHistory.ErrorMessage)
								msStartDate = sStartDate
								msEndDate = msStartDate
								If CreateUploadFile() Then
									If Not moProcessHistory.MarkEnd(msProcessDate, msCurrentProcessTime, mdtCurrentDateTime, msPathFileName, msStartDate, msEndDate, miRecordsRead, miRecordsWritten) Then Throw New Exception(moProcessHistory.ErrorMessage)
								End If
							Else
								TraceWrite("Already Processed: " & sStartDate, sRoutine)
							End If
						Next iDayCounter
					Else
						TraceWrite("Not Required: " & Format(CDate(Mid(msProcessDate, 5, 2) & "-" & Mid(msProcessDate, 7, 2) & "-" & Mid(msProcessDate, 1, 4)), "yyyy-MM-dd") & Space(1) & msCurrentProcessTime, sRoutine)
					End If
				Next iWork
      End If
			TraceWrite("End Processing", sRoutine)
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub
#End Region

#Region "CreateUploadFile"
	Private Function CreateUploadFile() As Boolean
		Dim sRoutine As String = "CreateUploadFile"
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim oDBC As New OracleConnection()
		Dim oCmd As New OracleCommand
		Dim oRdr As OracleDataReader = Nothing
		Dim sOutRecord As String

		Try
			TraceWrite("Start Processing " & msProcessDate & Space(1) & msCurrentProcessTime & ": " & msStartDate & " - " & msEndDate, sRoutine)
			CreateDirectory(gsUploadFileLocation)
			msPathFileName = gsUploadFileLocation & gsUploadFileName.Replace("????????", Mid(msEndDate, 6, 2) & Mid(msEndDate, 9, 2) & Mid(msEndDate, 1, 4))
			If File.Exists(msPathFileName) Then File.Delete(msPathFileName)
			If File.Exists(msPathFileName) Then Throw New Exception("Error deleteing upload file:" & msPathFileName)
			oFileStream = New FileStream(msPathFileName, FileMode.CreateNew, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			If Not OpenDBOracle(oDBC) Then Throw New Exception("Error opening database.")

			miRecordsRead = 0
			miRecordsWritten = 0

			sOutRecord = vbNullString
			AddField(sOutRecord, "CHG_SERVICE_DT")
			AddField(sOutRecord, "LOC_FAC")
			AddField(sOutRecord, "LOC_UNIT")
			AddField(sOutRecord, "ENCOUNTER")
			AddField(sOutRecord, "MRN")
			AddField(sOutRecord, "FINANCIAL_CLASS")
			AddField(sOutRecord, "RENDERING_PROVIDER")
			AddField(sOutRecord, "RENDERING_PROVIDER_NPI")
			AddField(sOutRecord, "ORDERING_PROVIDER")
			AddField(sOutRecord, "ORDERING_PROVIDER_NPI")
			AddField(sOutRecord, "CHG_ITEM_ID")
			AddField(sOutRecord, "CHG_ACTIVITY_DT")
			AddField(sOutRecord, "CHG_INTERFACE_DT")
			AddField(sOutRecord, "QTY")
			AddField(sOutRecord, "CHG_AMT")
			AddField(sOutRecord, "BILL_CODE")
			AddField(sOutRecord, "BILL_CODE_DESCRIPTION")
			AddField(sOutRecord, "MODIFIERS")
			AddField(sOutRecord, "DIAGNOSIS_CODES")
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = GEBillingDataSQL()
			oCmd.CommandType = CommandType.Text
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read
					miRecordsRead += 1
					sOutRecord = vbNullString
					AddField(sOutRecord, SetString(oRdr.Item("DateOfService").ToString))
					AddField(sOutRecord, SetString(oRdr.Item("PracticeName").ToString))
					AddField(sOutRecord, vbNullString)
					AddField(sOutRecord, SetString(oRdr.Item("VisitID").ToString))
					AddField(sOutRecord, SetString(oRdr.Item("MedicalRecordNumber").ToString))
					AddField(sOutRecord, vbNullString)
					AddField(sOutRecord, FormatName(SetString(oRdr.Item("RenderingProviderLastName").ToString), SetString(oRdr.Item("RenderingProviderFirstName").ToString)))
					AddField(sOutRecord, SetString(oRdr.Item("RenderingProviderNPI").ToString))
					AddField(sOutRecord, FormatName(SetString(oRdr.Item("OrderingProviderLastName").ToString), SetString(oRdr.Item("OrderingProviderFirstName").ToString)))
					AddField(sOutRecord, SetString(oRdr.Item("OrderingProviderNPI").ToString))
					AddField(sOutRecord, SetString(oRdr.Item("OrderID").ToString))
					AddField(sOutRecord, SetString(oRdr.Item("OrderDate").ToString))
					AddField(sOutRecord, SetString(oRdr.Item("DTSExportDate").ToString))
					AddField(sOutRecord, SetString(oRdr.Item("NumberOfVisits").ToString))
					AddField(sOutRecord, vbNullString)
					AddField(sOutRecord, SetString(oRdr.Item("OrderCode").ToString))
					AddField(sOutRecord, SetString(oRdr.Item("OrderDescription").ToString))
					AddField(sOutRecord, RemoveDuplicates(SetString(oRdr.Item("Modifiers").ToString), CChar("~")))
					AddField(sOutRecord, RemoveDuplicates(SetString(oRdr.Item("DiagnosisCodes").ToString), CChar("|")))
					oStreamWriter.WriteLine(sOutRecord)
					miRecordsWritten += 1
				End While
			End If
			TraceWrite("End Processing " & msProcessDate & Space(1) & msCurrentProcessTime & ": " & msStartDate & " - " & msEndDate, sRoutine)
			CreateUploadFile = True
		Catch oException As Exception
			CreateUploadFile = False
			TraceWrite("Error: " & oException.Message, sRoutine)
		Finally
			CloseOutput(oStreamWriter, oFileStream)
			CloseRDROracle(oRdr)
			CloseCMDOracle(oCmd)
			CloseDBOracle(oDBC)
		End Try
	End Function
#End Region
End Module
