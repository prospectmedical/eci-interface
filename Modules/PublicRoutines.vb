#Region "Init"
Option Strict On
Option Explicit On
Imports System.IO
Imports System.Reflection
#End Region

Module PublicRoutines
#Region "Calculations"
	'  Public Function DivideInt32AsInt32(Value01 As Int32, Value02 As Int32) As Int32
	'    If Value01 = 0 Or Value02 = 0 Then Return 0
	'    Return Convert.ToInt32(Value01 / Value02)
	'  End Function

	'  Public Function DivideInt32AsDouble(Value01 As Int32, Value02 As Int32) As Double
	'    If Value01 <> 0 AndAlso Value02 <> 0 Then
	'      DivideInt32AsDouble = Value01 / Value02
	'    Else
	'      DivideInt32AsDouble = 0
	'    End If
	'  End Function
	'#End Region

	'#Region "Set Routines"
	'	Public Function SetDouble(ByVal Value As Object) As Double
	'		If IsDBNull(Value) Then Return 0
	'		If IsNothing(Value) Then Return 0
	'		If Not IsNumeric(Value) Then Return 0
	'		Return CType(Value, Double)
	'	End Function

	Public Function SetInt32(ByVal Value As Object) As Int32
		If IsDBNull(Value) Then Return 0
		If IsNothing(Value) Then Return 0
		If Not IsNumeric(Value) Then Return 0
		Return CType(Value, Int32)
	End Function

	Public Function SetString(ByVal InString As Object) As String
		If IsDBNull(InString) Then Return ""
		If IsNothing(InString) Then Return ""
		Return Trim$(CType(InString, String))
	End Function

	'	Public Function SetDate(ByVal Value As Object) As String
	'		If IsDBNull(Value) Then Return vbNullString
	'		If IsNothing(Value) Then Return vbNullString
	'		If Not IsDate(Value) Then Return vbNullString
	'		Return Format$(CDate(Value), "MM/dd/yyyy")
	'	End Function

	'	Public Function SetDateTime(ByVal Value As Object) As Date
	'		If IsDBNull(Value) Then Return New Date
	'		If IsNothing(Value) Then Return New Date
	'		If Not IsDate(Value) Then Return New Date
	'		Return CDate(Value)
	'	End Function
#End Region

#Region "Format Routines"
	Public Function StringRepeat(ByVal Characters As String, ByVal RepeatValue As Int32) As String
		Dim iWork As Int32

		StringRepeat = vbNullString
		If RepeatValue <= 0 Then Exit Function
		For iWork = 1 To RepeatValue
			StringRepeat = StringRepeat & Characters
		Next
	End Function

	'	Public Function HoursMinutes(Minutes As Int32) As String
	'    Dim iHours As Int32
	'    Dim iMinutes As Int32 = Minutes

	'    If Minutes <= 0 Then Return vbNullString
	'    If Minutes < 60 Then Return ":" & Format(Minutes, "00")
	'    iHours = iMinutes \ 60
	'    iMinutes = iMinutes - (iHours * 60)
	'    HoursMinutes = Format(iHours, "#0") & ":" & Format(iMinutes, "00")
	'  End Function

	Public Function FormatName(InLast As String, InFirst As String, Optional InMiddle As String = vbNullString) As String
		Dim sWork, sFirstName, sMiddleInitial As String

		sWork = Trim$(InLast)
		sFirstName = Trim$(InFirst)
		sMiddleInitial = Trim$(InMiddle)

		If Len(sFirstName) > 0 Then
			If Len(sWork) > 0 Then sWork = sWork & ", "
			sWork = sWork & sFirstName
		End If
		If Len(sMiddleInitial) > 0 Then
			If Len(sWork) > 0 Then sWork = sWork & " "
			sWork = sWork & sMiddleInitial & "."
		End If
		FormatName = sWork
	End Function
#End Region

#Region "Path/File Routines"
	Public Function AddBackslash(ByRef InDriveDirectory As String) As String
		Dim sWork As String

		If InDriveDirectory = vbNullString Then Return vbNullString
		sWork = Trim(InDriveDirectory)
		If Mid(sWork, Len(sWork), 1) = "\" Then
			AddBackslash = sWork
		Else
			AddBackslash = sWork & "\"
		End If
	End Function

	Public Function ApplicationStartupPath() As String
		ApplicationStartupPath = Path.GetDirectoryName([Assembly].GetExecutingAssembly().Location)
		If UCase$(Mid$(ApplicationStartupPath, Len(ApplicationStartupPath) - 3, 4)) = "\BIN" Then
			ApplicationStartupPath = Mid$(ApplicationStartupPath, 1, Len(ApplicationStartupPath) - 3)
		End If
		If UCase$(Mid$(ApplicationStartupPath, Len(ApplicationStartupPath) - 9, 10)) = "\BIN\DEBUG" Then
			ApplicationStartupPath = Mid$(ApplicationStartupPath, 1, Len(ApplicationStartupPath) - 9)
		End If
		If UCase$(Mid$(ApplicationStartupPath, Len(ApplicationStartupPath) - 11, 12)) = "\BIN\RELEASE" Then
			ApplicationStartupPath = Mid$(ApplicationStartupPath, 1, Len(ApplicationStartupPath) - 11)
		End If
		AddBackslash(ApplicationStartupPath)
	End Function

	Public Function CreateDirectory(ByVal FileFullPath As String) As Boolean
		Dim oFileInfo As New IO.FileInfo(AddBackslash(SetString(FileFullPath)))
		If Not oFileInfo.Directory.Exists Then oFileInfo.Directory.Create()
		CreateDirectory = oFileInfo.Directory.Exists
		CloseFileInfo(oFileInfo)
	End Function

	'  Public Sub DeleteFile(PathFileName As String)
	'    Dim oFileInfo As FileInfo

	'    Try
	'      oFileInfo = New FileInfo(PathFileName)
	'      If oFileInfo.Exists Then oFileInfo.Delete()
	'    Catch oException As Exception
	'    Finally
	'      oFileInfo = Nothing
	'    End Try
	'  End Sub

	'	Public Function MappedDriveDelete(DriveLetter As String) As Boolean
	'		Dim oProcess As New Process

	'		Try
	'			oProcess.StartInfo.FileName = "net.exe"
	'			oProcess.StartInfo.Arguments = " use " & UCase(DriveLetter) & ": /delete /Y"
	'			oProcess.StartInfo.CreateNoWindow = True
	'			oProcess.Start()
	'			oProcess.WaitForExit()
	'			Return True
	'		Catch oException As Exception
	'			Return False
	'		Finally
	'		End Try
	'	End Function

	'	Public Function MappedDriveCreate(DriveLetter As String, UNC As String, UserID As String, Password As String) As Boolean
	'		Dim oProcess As New Process

	'		Try
	'			oProcess.StartInfo.FileName = "net.exe"
	'			oProcess.StartInfo.Arguments = " use " & DriveLetter & ": " & UNC & " " & Password & " /USER:" & UserID
	'			oProcess.StartInfo.CreateNoWindow = True
	'			oProcess.Start()
	'			oProcess.WaitForExit()
	'			Return True
	'		Catch oException As Exception
	'			Return False
	'		Finally
	'		End Try
	'	End Function

	Public Function CloseInput(oStreamReader As StreamReader, oFileStream As FileStream) As Boolean
		Try
			oStreamReader.Close()
			oFileStream.Close()
			Return True
		Catch oException As Exception
			Return False
		Finally
		End Try
	End Function

	Public Function CloseOutput(oStreamWriter As StreamWriter, oFileStream As FileStream) As Boolean
		Try
			oStreamWriter.Close()
			oFileStream.Close()
			Return True
		Catch oException As Exception
			Return False
		Finally
		End Try
	End Function
#End Region

#Region "Logging"
	Public Sub WriteEventLog(ByVal Message As String, ByVal Category As String, ByVal EventType As EventLogEntryType)
		Dim oEventLog As New System.Diagnostics.EventLog

		Try
			If Not System.Diagnostics.EventLog.Exists(gsEventLogName) Then
				System.Diagnostics.EventLog.CreateEventSource(gsApplicationName, gsEventLogName)
			End If
			oEventLog.Source = gsEventLogName
			oEventLog.WriteEntry("[" & Category & "] " & Message, EventType)
		Catch oException As Exception
		Finally
		End Try
	End Sub

	Public Sub TraceWrite(ByVal Message As String, ByVal Category As String)
		Dim sCategory As String = Category

		If Len(sCategory) < 30 Then sCategory = sCategory & StringRepeat(".", 30 - Len(sCategory))
		If gbTrace Then
			Trace.WriteLine(Format$(DateTime.Now, "MM/dd/yyyy HH:mm:ss.fff") & " - " & Message, sCategory)
		End If
	End Sub

	Public Sub WriteEventTrace(ByVal Message As String, ByVal Category As String, ByVal EventType As EventLogEntryType) 'As Boolean
		WriteEventLog(Message, Category, EventType)
		TraceWrite(Message, Category)
	End Sub
#End Region

#Region "Close Routines"
	Public Sub CloseFileInfo(ByVal InFileInfo As FileInfo)
		Try
			InFileInfo = Nothing
		Catch oException As Exception
		Finally
		End Try
	End Sub

	Public Sub KillExcel()
		Dim oProcesses() As Process = Nothing
		oProcesses = System.Diagnostics.Process.GetProcessesByName("Excel")
		For Each oProcess As Process In oProcesses
			oProcess.Kill()
		Next
	End Sub
#End Region

#Region "User Routines"
	Public Function GetUser(ByVal InUser As String) As String
		Dim iWork As Int32
		Dim sWork As String

		sWork = Trim$(InUser)
TryAgain:
		iWork = InStr(1, sWork, "\")
		If iWork <= 0 Then
			Return sWork
		Else
			If iWork = Len(sWork) Then
				sWork = vbNullString
			Else
				sWork = Mid$(sWork, iWork + 1, Len(sWork))
				GoTo TryAgain
			End If
		End If
		Return vbNullString
	End Function
#End Region

#Region "eMail Routines"
	Public Function Disclaimer() As String
		Return "***** NOTE: Do not reply to this email!" & "<br />" & StringRepeat("&nbsp;", 24) & "This email address is not monitored." & "<br />"
	End Function
#End Region
End Module