﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Threading
#End Region

Module EMailRoutines
#Region "Routines"
  Private Function StartUpEMailMessage() As String
    StartUpEMailMessage = "The " & gsApplicationName & " Service has been started." & "<br />"
    StartUpEMailMessage = StartUpEMailMessage & "__________________________________________________________________________________________"
    StartUpEMailMessage = StartUpEMailMessage & "<br />" & "<br />"
    StartUpEMailMessage = StartUpEMailMessage & "END OF REPORT" & "<br />" & "<br />" & Disclaimer() & "<br />"
  End Function

  Private Function ShutDownEMailMessage() As String
    ShutDownEMailMessage = "The " & gsApplicationName & " Service has been shutdown." & "<br />"
    ShutDownEMailMessage = ShutDownEMailMessage & "__________________________________________________________________________________________"
    ShutDownEMailMessage = ShutDownEMailMessage & "<br />" & "<br />"
    ShutDownEMailMessage = ShutDownEMailMessage & "See attached service log." & "<br />" & "END OF REPORT" & "<br />" & "<br />" & Disclaimer() & "<br />"
  End Function

  Private Function MessageBody(ByVal Message As String) As String
    MessageBody = Message & "<br />"
    MessageBody = MessageBody & "__________________________________________________________________________________________"
    MessageBody = MessageBody & "<br />" & "<br />"
    MessageBody = MessageBody & "END OF REPORT" & "<br />" & "<br />" & Disclaimer() & "<br />"
  End Function
#End Region

#Region "StartUpEMail"
  Public Sub StartUpEMail()
    Dim oSMTPEMail As New SMTPEMail.Mail

    Try
      If Not oSMTPEMail.Recipient(gsEMailRecipient) Then Throw New Exception("Recipient EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Sender(gsEMailSender) Then Throw New Exception("Sender EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Subject(gsApplicationName & " Service Start Up") Then Throw New Exception("Subject EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Message(StartUpEMailMessage) Then Throw New Exception("Message EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Send(gsSMTPAddress) Then Throw New Exception("Send EMail Error: " & oSMTPEMail.ErrorMessage)
      Thread.Sleep(giEMailPauseTime)
    Catch oException As Exception
      WriteEventTrace(oException.Message, "Process/StartUpEMail", EventLogEntryType.Error)
    Finally
      oSMTPEMail = Nothing
    End Try
  End Sub
#End Region

#Region "ShutDownEMail"
  Public Sub ShutDownEMail()
    Dim oSMTPEMail As New SMTPEMail.Mail

    Try
      If Not oSMTPEMail.Recipient(gsEMailRecipient) Then Throw New Exception("Recipient EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Sender(gsEMailSender) Then Throw New Exception("Sender EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Subject(gsApplicationName & " Service ShutDown") Then Throw New Exception("Subject EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Message(ShutDownEMailMessage) Then Throw New Exception("Message EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.AddAttachment(AddBackslash(gsLogPath) & gsLogFileName) Then Throw New Exception("AddAttachment EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Send(gsSMTPAddress) Then Throw New Exception("Send EMail Error: " & oSMTPEMail.ErrorMessage)
      Thread.Sleep(giEMailPauseTime)
    Catch oException As Exception
      WriteEventTrace(oException.Message, "Process/ShutDownEMail", EventLogEntryType.Error)
    Finally
      oSMTPEMail = Nothing
    End Try
  End Sub
#End Region

#Region "SendProcessingEMail"
  Public Sub SendProcessingEMail(ByVal Subject As String, ByVal Message As String)
    Dim oSMTPEMail As New SMTPEMail.Mail

    Try
      If Not oSMTPEMail.Recipient(gsEMailRecipient) Then Throw New Exception("Recipient EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Sender(gsEMailSender) Then Throw New Exception("Sender EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Subject(Subject) Then Throw New Exception("Subject EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Message(MessageBody(Message)) Then Throw New Exception("Message EMail Error: " & oSMTPEMail.ErrorMessage)
      If Not oSMTPEMail.Send(gsSMTPAddress) Then Throw New Exception("Send EMail Error: " & oSMTPEMail.ErrorMessage)
      Thread.Sleep(giEMailPauseTime)
    Catch oException As Exception
      WriteEventTrace(oException.Message, "SendProcessingEMail", EventLogEntryType.Error)
    Finally
      oSMTPEMail = Nothing
    End Try
  End Sub
#End Region

#Region "SendEMail"
  Public Sub SendEMail(ByVal Subject As String, ByVal Message As String, Recipients As String)
    Dim oSMTPEMail As SMTPEMail.Mail
    Dim sRecipient As String
    Dim saRecipients As String()

    Try
      saRecipients = Recipients.Split(New Char() {","c})
      For Each sRecipient In saRecipients
        oSMTPEMail = New SMTPEMail.Mail
        If Not oSMTPEMail.Recipient(sRecipient) Then Throw New Exception("Recipient EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Sender(gsEMailSender) Then Throw New Exception("Sender EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Subject(Subject) Then Throw New Exception("Subject EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Message(MessageBody(Message)) Then Throw New Exception("Message EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Send(gsSMTPAddress) Then Throw New Exception("Send EMail Error: " & oSMTPEMail.ErrorMessage)
        Thread.Sleep(giEMailPauseTime)
      Next
    Catch oException As Exception
      WriteEventTrace(oException.Message, "SendEMail", EventLogEntryType.Error)
    Finally
      oSMTPEMail = Nothing
    End Try
  End Sub
#End Region
End Module
