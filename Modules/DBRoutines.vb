#Region "Init"
Option Strict On
Option Explicit On
Imports System.Data.OleDb
Imports Oracle.ManagedDataAccess.Client
#End Region

Module DBRoutines
#Region "Declarations"

#End Region

#Region "DataBaseConnectionString"
	Public Function DataBaseConnectionString(ByVal DataBaseUser As String, ByVal DatabasePassword As String, ByVal DatabaseName As String, ByVal Server As String) As String
		Return "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & DataBaseUser & ";Password=" & DatabasePassword & ";Initial Catalog=" & DatabaseName &
						";Data Source=" & Server & ";Use Procedure for Prepare=1;" & "Auto Translate=True;Packet Size=4096;Workstation ID=AIApplication;Use Encryption for Data=False;" &
						"Tag with column collation when possible=False"
	End Function

	Public Function DataBaseConnectionStringOracle(ByVal DataBaseUser As String, ByVal DatabasePassword As String, ByVal DatabaseName As String, ByVal Server As String) As String
		Return "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & Server & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & DatabaseName & ")));User Id=" & DataBaseUser & ";Password=" & DatabasePassword & ";"
	End Function
#End Region

#Region "OpenDB"
	Public Function OpenDB(DatabaseConnection As OleDbConnection) As Boolean
		Try
			DatabaseConnection.ConnectionString = gsECIConnectionString
			DatabaseConnection.Open()
			OpenDB = True
		Catch oException As Exception
			OpenDB = False
		Finally
		End Try
	End Function
#End Region

#Region "OpenDBOracle"
	Public Function OpenDBOracle(DatabaseConnection As OracleConnection) As Boolean
		Try
			DatabaseConnection.ConnectionString = gsEMRConnectionString
			DatabaseConnection.Open()
			OpenDBOracle = True
		Catch oException As Exception
			OpenDBOracle = False
		Finally
		End Try
	End Function
#End Region

#Region "CloseDB"
	Public Sub CloseDB(ByVal cnDB As OleDbConnection)
    Try
      cnDB.Close()
      cnDB.Dispose()
      cnDB = Nothing
    Catch oException As Exception
    End Try
  End Sub
#End Region

#Region "CloseRDR"
  Public Sub CloseRDR(ByVal oRDR As OleDbDataReader)
    Try
      oRDR.Close()
      oRDR = Nothing
    Catch oException As Exception
    End Try
  End Sub
#End Region

#Region "CloseCMD"
  Public Sub CloseCMD(ByVal oCMD As OleDbCommand)
    Try
      oCMD.Dispose()
    Catch oException As Exception
    End Try
  End Sub
#End Region

#Region "CloseDBOracle"
	Public Sub CloseDBOracle(ByVal cnDB As OracleConnection)
		Try
			cnDB.Close()
			cnDB.Dispose()
			cnDB = Nothing
		Catch oException As Exception
		End Try
	End Sub
#End Region

#Region "CloseRDROracle"
	Public Sub CloseRDROracle(ByVal oRDR As OracleDataReader)
		Try
			oRDR.Close()
			oRDR.Dispose()
			oRDR = Nothing
		Catch oException As Exception
		End Try
	End Sub
#End Region

#Region "CloseCMDOracle"
	Public Sub CloseCMDOracle(ByVal oCMD As OracleCommand)
		Try
			oCMD.Dispose()
			oCMD = Nothing
		Catch oException As Exception
		End Try
	End Sub
#End Region
End Module
