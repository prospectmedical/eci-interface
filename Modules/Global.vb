#Region "Init"
Option Strict On
Option Explicit On
#End Region

Module [Global]
#Region "Variables"
	Public gsAppPath, gsVersion, gsWindowsUserID, gsLogPath, gsEMailRecipient, gsEMailSender, gsEMRConnectionString, gsECIConnectionString, gsUploadFileLocation, gsUploadFileName As String
	Public gsLogFileName, gsEventLogName As String
	Public gbTrace, gbSendEMail As Boolean
	Public giServicePauseTime As Int32
	Public galProcessTime As ArrayList
#End Region

#Region "Constants"
	Public Const gsApplicationName As String = "ECI"
	Public Const gsEntityDescription As String = "Crozer-Keystone Health System, Inc."
	Public Const giEMailPauseTime As Int32 = 2000
	Public Const gsSMTPAddress As String = "smtp.crozer.org"
	Public Const giAccountID As Int32 = 1
#End Region
End Module