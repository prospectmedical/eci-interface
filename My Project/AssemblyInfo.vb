﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ECI")>
<Assembly: AssemblyDescription("Gathers billing data from the EMR to validate billing on the CPM side.")>
<Assembly: AssemblyCompany("Crozer-Keystone Health System, Inc.")>
<Assembly: AssemblyProduct("ECI")>
<Assembly: AssemblyCopyright("Copyright © Crozer-Keystone Health System 2017")>
<Assembly: AssemblyTrademark("Crozer-Keystone Health System 2017")>

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c5158af1-0309-4c15-a6b2-a8aa03940631")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.3.2018.0219")>
<Assembly: AssemblyFileVersion("2.3.2018.0219")>
