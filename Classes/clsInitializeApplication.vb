#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Public Class InitializeApplication
#Region "Declarations"
	Private msLogPath, msEMailRecipient, msEMailSender, msUploadFileLocation, msUploadFileName As String
	Private mbTrace, mbSendEMail As Boolean
  Private miServicePauseTime As Int32
#End Region

#Region "Class Properties"
	Public ReadOnly Property LogPath As String
		Get
			Return msLogPath
		End Get
	End Property

	Public ReadOnly Property Trace As Boolean
		Get
			Return mbTrace
		End Get
	End Property

	Public ReadOnly Property ServicePauseTime As Int32
		Get
			Return miServicePauseTime
		End Get
	End Property

	Public ReadOnly Property EMailRecipient As String
		Get
			Return msEMailRecipient
		End Get
	End Property

	Public ReadOnly Property EMailSender As String
		Get
			Return msEMailSender
		End Get
	End Property

	Public ReadOnly Property SendEMail As Boolean
		Get
			Return mbSendEMail
		End Get
	End Property

	Public ReadOnly Property UploadFileLocation As String
		Get
			Return msUploadFileLocation
		End Get
	End Property

	Public ReadOnly Property UploadFileName As String
		Get
			Return msUploadFileName
		End Get
	End Property
#End Region

#Region "Initialize"
	Private Sub Initialize()
		msLogPath = "C:\Service Logs\ECI\"
		mbTrace = True
		miServicePauseTime = 60000
		msEMailRecipient = vbNullString
		msEMailSender = vbNullString
		mbSendEMail = False
	End Sub
#End Region

#Region "New"
	Public Sub New()
    Initialize()
  End Sub
#End Region

#Region "GetSettings"
  Public Function GetSettings(INIPathFileName As String) As Boolean
		Dim oFileStream As New FileStream(INIPathFileName, FileMode.Open, FileAccess.Read)
		Dim oStreamReader As New StreamReader(oFileStream)
		Dim sInRecord, sItemType, sItemSetting, sWork As String
		Dim iWork As Int32
		Dim iINICounter As Int32 = 0
		Const iINICount As Int32 = 9
		Dim bEMailRecipientFound As Boolean = False
		Dim bProcessTimeFound As Boolean = False

		Try
      msEMailRecipient = vbNullString
			GetSettings = False
			galProcessTime = New ArrayList
			oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)
			While oStreamReader.Peek() > -1
				sInRecord = Trim$(oStreamReader.ReadLine())
        If Len(sInRecord) > 0 Then
          If Mid$(sInRecord, 1, 1) <> ";" Then
            iWork = InStr(1, sInRecord, ";")
            If iWork > 0 Then sInRecord = Trim$(Mid$(sInRecord, 1, iWork - 1))
            iWork = InStr(1, sInRecord, " ")
            If iWork > 3 Then
              If iWork < Len(sInRecord) Then
                sItemType = Trim$(LCase$(Mid$(sInRecord, 1, iWork - 1)))
                sItemSetting = Trim$(Mid$(sInRecord, iWork + 1, Len(sInRecord)))
                Select Case sItemType
                  Case "[logpath]"
                    msLogPath = sItemSetting
                    iINICounter += 1
                  Case "[trace]"
                    If LCase$(SetString(sItemSetting)) = "true" Then
                      mbTrace = True
                    Else
                      mbTrace = False
                    End If
                    iINICounter += 1
                  Case "[servicepausetime]"
                    miServicePauseTime = SetInt32(sItemSetting) * 60000
                    iINICounter += 1
                  Case "[emailrecipient]"
                    sWork = SetString(sItemSetting)
                    If Len(msEMailRecipient) > 0 Then
                      If Mid(msEMailRecipient, Len(msEMailRecipient), 1) <> "," AndAlso Mid(msEMailRecipient, Len(msEMailRecipient), 1) <> ";" Then msEMailRecipient = msEMailRecipient & ";"
                    End If
                    msEMailRecipient = msEMailRecipient & Replace(sWork, ",", ";")
										If Not bEMailRecipientFound Then
											iINICounter += 1
											bEMailRecipientFound = True
										End If
									Case "[emailsender]"
                    msEMailSender = SetString(sItemSetting)
                    iINICounter += 1
                  Case "[sendemail]"
                    If LCase$(SetString(sItemSetting)) = "true" Then
                      mbSendEMail = True
                    Else
                      mbSendEMail = False
                    End If
										iINICounter += 1
									Case "[uploadfilelocation]"
										msUploadFileLocation = AddBackslash(sItemSetting)
										iINICounter += 1
									Case "[uploadfilename]"
										msUploadFileName = sItemSetting
										iINICounter += 1
									Case "[processingtime]"
										galProcessTime.Add(sItemSetting)
										If Not bProcessTimeFound Then
											iINICounter += 1
											bProcessTimeFound = True
										End If
								End Select
              End If
            End If
          End If
        End If
			End While
			CloseInput(oStreamReader, oFileStream)
			If iINICount <> iINICounter Then
        WriteEventLog("InitializeApplication", "Error: Incorrect ini count, entries are missing.", EventLogEntryType.Error)
      Else
        GetSettings = True
      End If
    Catch oException As Exception
      WriteEventLog("InitializeApplication", "Error: " & oException.Message, EventLogEntryType.Error)
      GetSettings = False
    Finally
    End Try
  End Function
#End Region

#Region "Version Check"
  Public ReadOnly Property VersionCheck(ByVal Version As String) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim bWork As Boolean

			If Not OpenDB(oDBC) Then Return False
			oCmd.CommandText = "sVersionCheck"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@Version", OleDbType.VarChar, 25).Value = Version
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader
        If oRdr.HasRows Then
          bWork = True
        Else
          bWork = False
        End If
      Catch oException As Exception
        bWork = False
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bWork
    End Get
  End Property
#End Region
End Class