﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class ProcessHistory
#Region "Declartions"
  Private miProcessHistoryID As Int32
  Private msErrorMessage As String
#End Region

#Region "Class Properties"
  Public ReadOnly Property ProcessHistoryID() As Int32
    Get
      Return miProcessHistoryID
    End Get
  End Property

  Public ReadOnly Property ErrorMessage As String
    Get
      Return msErrorMessage
    End Get
  End Property
#End Region

#Region "Complete"
	Public Function Complete(ProcessTypeID As Int32, ProcessDate As String, ProcessTime As String) As Boolean
		Dim sRoutine As String = "Complete"
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Processed-Complete: Error opening database.")
			oCmd.CommandText = "sProcessHistoryComplete"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessTypeID", OleDbType.BigInt).Value = ProcessTypeID
			oCmd.Parameters.Add("@ProcessDate", OleDbType.VarChar, 8).Value = ProcessDate
			oCmd.Parameters.Add("@ProcessTime", OleDbType.VarChar, 5).Value = ProcessTime
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			Complete = oRdr.HasRows
		Catch oException As Exception
			msErrorMessage = "ProcessHistory." & sRoutine & ": " & oException.Message
			Complete = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "DateComplete"
	Public Function DateComplete(ProcessTypeID As Int32, StartDate As String) As Boolean
		Dim sRoutine As String = "DateComplete"
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Processed-DateComplete: Error opening database.")
			oCmd.CommandText = "sProcessHistoryDateComplete"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessTypeID", OleDbType.BigInt).Value = ProcessTypeID
			oCmd.Parameters.Add("@StartDate", OleDbType.VarChar, 10).Value = StartDate
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			DateComplete = oRdr.HasRows
		Catch oException As Exception
			msErrorMessage = "ProcessHistory." & sRoutine & ": " & oException.Message
			DateComplete = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "MarkStart"
	Public Function MarkStart(ProcessTypeID As Int32, StartDateTime As Date) As Boolean
		Dim sRoutine As String = "MarkStart"
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		MarkStart = False
		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "iProcessHistory"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessTypeID", OleDbType.BigInt).Value = ProcessTypeID
			oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
			oCmd.Parameters.Add("@StartDateTime", OleDbType.Date).Value = StartDateTime
			oCmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			miProcessHistoryID = SetInt32(oCmd.Parameters("@NewID").Value)
			MarkStart = True
		Catch oException As Exception
			msErrorMessage = "ProcessHistory." & sRoutine & ": " & oException.Message
			MarkStart = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "MarkEnd"
	Public Function MarkEnd(ProcessDate As String, ProcessTime As String, EndDateTime As Date, PathFileName As String, StartDate As String, EndDate As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
		Dim sRoutine As String = "MarkEnd"
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		MarkEnd = False
		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "uProcessHistory"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessHistoryID", OleDbType.BigInt).Value = miProcessHistoryID
			oCmd.Parameters.Add("@ProcessDate", OleDbType.VarChar, 8).Value = ProcessDate
			oCmd.Parameters.Add("@ProcessTime", OleDbType.VarChar, 5).Value = ProcessTime
			oCmd.Parameters.Add("@EndDateTime", OleDbType.Date).Value = EndDateTime
			oCmd.Parameters.Add("@PathFileName", OleDbType.VarChar, 200).Value = PathFileName
			oCmd.Parameters.Add("@StartDate", OleDbType.VarChar, 10).Value = StartDate
			oCmd.Parameters.Add("@EndDate", OleDbType.VarChar, 10).Value = EndDate
			oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
			oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			MarkEnd = True
		Catch oException As Exception
			msErrorMessage = "ProcessHistory." & sRoutine & ": " & oException.Message
			MarkEnd = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region
End Class

