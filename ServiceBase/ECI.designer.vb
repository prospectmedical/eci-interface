#Region "Init"
Option Explicit On
Option Strict On
#End Region

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class ECI
#Region "Declarations"
	Inherits System.ServiceProcess.ServiceBase
	Private oIContainer As System.ComponentModel.IContainer
#End Region

#Region "Routines"
	<MTAThread()> <System.Diagnostics.DebuggerNonUserCode()> Public Shared Sub Main()

		'Do not delete
		'This code will delete the application specific event log
		'System.Diagnostics.EventLog.Delete(gsDefaultApplicationName & " Service")
		'System.Diagnostics.EventLog.DeleteEventSource(gsDefaultApplicationName)
		'Do not delete

		gsEventLogName = "ECI"
		StartService(moTextWriterTraceListener)
	End Sub
#End Region

#Region "Service Control"
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso oIContainer IsNot Nothing Then
			oIContainer.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.CanShutdown = True
		Me.ServiceName = "ECI"
	End Sub
#End Region
End Class
